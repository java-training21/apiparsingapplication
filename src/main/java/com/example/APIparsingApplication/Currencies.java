package com.example.APIparsingApplication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component; //dodane

import java.util.Scanner;

//https://www.youtube.com/watch?v=Vfq2lP_JHu4
//https://www.youtube.com/watch?v=EDa2BrUgTzw&list=PL07TSwd5SmKZQMNEci1mHRnMFBKWXvACH&index=19
//https://www.jsonschema2pojo.org/
// ASCIIART: https://patorjk.com/software/taag/#p=display&h=3&f=Big%20Money-sw&t=

//Aplikacja integruje się z zewnętrzym API banki NBP i pobiera interesujace nas wartości
//Aplikacja korzysta z Jackosn i Jersey
//Nie w Spring



public class Currencies {

    public static void main(String[] args) {


        try {
            //Client client = new Client.create();
            Client client = new Client();

            System.out.println(" __    __ __    __ _______   ______  __      __        __       __  ______  __       __    __ ________ ");
            System.out.println("/  |  /  /  |  /  /       \\ /      \\/  \\    /  |      /  |  _  /  |/      \\/  |     /  |  /  /        |");
            System.out.println("$$ | /$$/$$ |  $$ $$$$$$$  /$$$$$$  $$  \\  /$$/       $$ | / \\ $$ /$$$$$$  $$ |     $$ |  $$ $$$$$$$$/ ");
            System.out.println("$$ |/$$/ $$ |  $$ $$ |__$$ $$ \\__$$/ $$  \\/$$/        $$ |/$  \\$$ $$ |__$$ $$ |     $$ |  $$ |  $$ |   ");
            System.out.println("$$  $$<  $$ |  $$ $$    $$<$$      \\  $$  $$/         $$ /$$$  $$ $$    $$ $$ |     $$ |  $$ |  $$ |   ");
            System.out.println("$$$$$  \\ $$ |  $$ $$$$$$$  |$$$$$$  |  $$$$/          $$ $$/$$ $$ $$$$$$$$ $$ |     $$ |  $$ |  $$ |   ");
            System.out.println("$$ |$$  \\$$ \\__$$ $$ |  $$ /  \\__$$ |   $$ |          $$$$/  $$$$ $$ |  $$ $$ |_____$$ \\__$$ |  $$ |   ");
            System.out.println("$$ | $$  $$    $$/$$ |  $$ $$    $$/    $$ |          $$$/    $$$ $$ |  $$ $$       $$    $$/   $$ |   ");
            System.out.println("$$/   $$/ $$$$$$/ $$/   $$/ $$$$$$/     $$/           $$/      $$/$$/   $$/$$$$$$$$/ $$$$$$/    $$/    ");
            System.out.println("                                                                                                       ");
            System.out.println("                                                                                                       ");
            System.out.println("                                                                                                       ");


            System.out.println("######################################################");
            System.out.println("########## APLIKACJA - KURSY WALUT Bank NBP ##########");
            System.out.println("Podaj datę [YYYY-MM-DD]: ");
            Scanner scannerDate = new Scanner(System.in);
            String urlFirst = scannerDate.nextLine();
            String url = "http://api.nbp.pl/api/exchangerates/tables/a/" + urlFirst + "/";
            System.out.println(url);
            WebResource webResource = client.resource(url); // //http://api.nbp.pl/api/exchangerates/tables/a/today/

            ClientResponse webResponse = webResource.accept("application/json").get(ClientResponse.class);

            if(webResponse.getStatus() != 200) {
                throw new RuntimeException("HTTP Error ..." + webResponse.getStatus());
            }

            String json = webResponse.getEntity(String.class);
            System.out.println(json);

            int lengthOfString = json.length();
            String json2 = json.substring(1, lengthOfString-1);
            System.out.println(json2); //to remove first and the last characters '[' and ']'
            System.out.println("");
            System.out.println("Lista kodów walut: ");


            for(int i=0;i<34;i++) {
                String[] tabCurrency = new String[34];
                String[] tabCode = new String[34];

                ObjectMapper mapper = new ObjectMapper();
                CurrencyRate currencyRate = mapper.readValue(json2, CurrencyRate.class);

                tabCurrency[i] = currencyRate.getRates().get(i).getCurrency();
                tabCode[i] = currencyRate.getRates().get(i).getCode();

                System.out.println("Kod waluty: " + tabCode[i]+" -> " + tabCurrency[i]);
            }


            int x = 0;
            while(x == 0) {

                Start start = new Start();
                System.out.println("Podaj kod waluty: ");
                Scanner scanner = new Scanner(System.in);
                String currencyX = scanner.nextLine();
                int result = start.exampleOfSwitch(currencyX);
                //System.out.println(result);


                //uzycie Jackson'a 2  https://en.wikipedia.org/wiki/Jackson_(API)
                ObjectMapper mapper = new ObjectMapper();
                CurrencyRate currencyRate = mapper.readValue(json2, CurrencyRate.class);


                System.out.println("#################################");
                System.out.println("Waluta: " + currencyRate.getRates().get(result).getCurrency());
                System.out.println("Kod waluty: " + currencyRate.getRates().get(result).getCode());
                System.out.println("Kurs [" + currencyRate.getRates().get(result).getCode() +"->PLN]: " + currencyRate.getRates().get(result).getMid());
                System.out.println("Kurs [PLN->" + currencyRate.getRates().get(result).getCode() +"]: " + 1/currencyRate.getRates().get(result).getMid());
                System.out.println("Tabela: " + currencyRate.getTable());
                System.out.println("Data kursu: " + currencyRate.getEffectiveDate());
                System.out.println("#################################");
                System.out.println("");

                System.out.println("Zakończyć wyszukiwanie? [yas:1 or no:0]: ");
                int temp = 0;
                temp = scanner.nextInt();

                if(temp==1)
                    x++;

            }

            System.out.println("  ______   ______   ______  _______         _______  __      __ ________ ");
            System.out.println(" /      \\ /      \\ /      \\/       \\       /       \\/  \\    /  /        |");
            System.out.println("/$$$$$$  /$$$$$$  /$$$$$$  $$$$$$$  |      $$$$$$$  $$  \\  /$$/$$$$$$$$/ ");
            System.out.println("$$ | _$$/$$ |  $$ $$ |  $$ $$ |  $$ |      $$ |__$$ |$$  \\/$$/ $$ |__    ");
            System.out.println("$$ |/    $$ |  $$ $$ |  $$ $$ |  $$ |      $$    $$<  $$  $$/  $$    |   ");
            System.out.println("$$ |$$$$ $$ |  $$ $$ |  $$ $$ |  $$ |      $$$$$$$  |  $$$$/   $$$$$/    ");
            System.out.println("$$ \\__$$ $$ \\__$$ $$ \\__$$ $$ |__$$ |      $$ |__$$ |   $$ |   $$ |_____ ");
            System.out.println("$$    $$/$$    $$/$$    $$/$$    $$/       $$    $$/    $$ |   $$       |");
            System.out.println(" $$$$$$/  $$$$$$/  $$$$$$/ $$$$$$$/        $$$$$$$/     $$/    $$$$$$$$/ ");
            System.out.println("");
            System.out.println("");

            System.out.println(" __    __                                  _______                    __          ");
            System.out.println("/  \\  /  |                                /       \\                  /  |         ");
            System.out.println("$$  \\ $$ | ______   ______  ______        $$$$$$$  |__    __ _______ $$/ __    __ ");
            System.out.println("$$$  \\$$ |/      \\ /      \\/      \\       $$ |__$$ /  |  /  /       \\/  /  |  /  |");
            System.out.println("$$$$  $$ |$$$$$$  /$$$$$$  $$$$$$  |      $$    $$<$$ |  $$ $$$$$$$  $$ $$ |  $$ |");
            System.out.println("$$ $$ $$ |/    $$ $$ |  $$//    $$ |      $$$$$$$  $$ |  $$ $$ |  $$ $$ $$ |  $$ |");
            System.out.println("$$ |$$$$ /$$$$$$$ $$ |    /$$$$$$$ |      $$ |__$$ $$ \\__$$ $$ |  $$ $$ $$ \\__$$ |");
            System.out.println("$$ | $$$ $$    $$ $$ |    $$    $$ |      $$    $$/$$    $$ $$ |  $$ $$ $$    $$/ ");
            System.out.println("$$/   $$/ $$$$$$$/$$/      $$$$$$$/       $$$$$$$/  $$$$$$$ $$/   $$/$$/ $$$$$$/  ");
            System.out.println("                                                   /  \\__$$ |                     ");
            System.out.println("                                                   $$    $$/                      ");
            System.out.println("                                                   $$    $$/                      ");
            System.out.println("                                                    $$$$$$/                       ");
            System.out.println("");


        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}