package com.example.APIparsingApplication;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class TestRestTemplateClass {

    public String callGet() {
        RestTemplate rest = new RestTemplate();
        ResponseEntity<String> exchange = rest.exchange("http://api.nbp.pl/api/exchangerates/tables/a/2022-11-23/",
                HttpMethod.GET,
                HttpEntity.EMPTY,
                String.class);
        return exchange.getBody();

    }
}
